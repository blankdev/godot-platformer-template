extends Node

const GRAVITY = 500
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_FRICTION = 5
const ACCELERATION = 0.5
const AIR_ACCELERATION = 0.1
const JUMP_FORCE = 150
const AIR_TIME_MAX = 10
const MOVEMENT_SPEED = 50
const knockSpeed = 100

func _push_crate(target):
	###	get collision normal on horizontal axis to know if player is on the right or left
	var normal = get_collision_normal()
	###	if on the left apply the force in the right direction
	if normal == Vector2(-1, 0):
		target.apply_impulse(Vector2(0, 0), Vector2(20, -10))
	###	if on the right apply the force in the left direction
	if normal == Vector2(1, 0):
		target.apply_impulse(Vector2(0, 0), Vector2(-20, -10))
	###	I also added a bit of upwards force to make sure the Box doesn't get stuck on the Ground while moving
	
	
func _follow_player(Player, Follower):
	
	###	Get Vector2 direction to the player global pos
	var direction = (Player.get_global_pos() - get_global_pos()).normalized()
	var DIR = Vector2()
	###	Get horizontal direction based on the direction to player
	if direction.x > 0:
		DIR = Vector2(1, 0)
	elif direction.x < 0:
		DIR = Vector2(-1, 0)
	else:
		DIR = Vector2(0, 0)
	
	###	Follow the player in the direction
	var motion = DIR * 35
	if Follower.can_move:
		move_and_slide(motion, FLOOR_NORMAL, SLOPE_FRICTION)
	
###	Function to push a character with target statement, make sure target has BEING_KNOCKED state and knockVelocity
func _push_character(target):
	###	Get the normal of collision
	var normal = get_collision_normal()
	###	TARGET ON LEFT SIDE
	if normal == Vector2(-1, 0):
		target.BEING_KNOCKED = true
		target.knockVelocity = Vector2(1, -1)
	###	TARGET ON RIGHT SIDE
	elif normal == Vector2(1, 0):
		target.BEING_KNOCKED = true
		target.knockVelocity = Vector2(-1, -1)
	###	IF ON TOP OR BOTTOM SO THEY CAN JUMP ON EACH OTHER
	else:
		target.BEING_KNOCKED = false
	
func _push_enemy(target):
	###	If the attack hitbox is empty
	if target == null:
		return
	###	Otherwise knock and damage the target
	else:
		target.BEING_KNOCKED = true
		var dir = ( target.get_pos() - get_pos()).normalized()
		###	TARGET ON LEFT SIDE
		if dir > Vector2(0, 1):
			target.BEING_KNOCKED = true
			target.knockVelocity = Vector2(1, -1)
		###	TARGET ON RIGHT SIDE
		elif dir < Vector2(0,-1):
			target.BEING_KNOCKED = true
			target.knockVelocity = Vector2(-1, -1)
			