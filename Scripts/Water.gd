extends Area2D



###	When a character falls into water
func _on_Area2D_body_enter( body ):
	var groups = body.get_groups()
	if groups.has("Character"):
		body.in_Water = true
		body.velocity.y = 0
	
### When a character moves out of the water
func _on_Area2D_body_exit( body ):
	var groups = body.get_groups()
	if groups.has("Character"):
		body.in_Water = false
