extends "res://Scripts/Characters.gd"

#	Physics
var velocity = Vector2()
var knockVelocity = Vector2()
#	States
var is_jumping = false
var BEING_KNOCKED = false
var can_move = true
var in_Water = false
#	Targets
var Player
#	Timers
var AIR_TIME = 0
var MOVE_PAUSE_TIMER = 0
var KNOCK_TIMER = 0
var TIME_COUNTER = 0
var MOVE_PAUSE_MAX = 60	#	Movement pause after hitting the Player character 
var AIR_TIME_MAX = 12	#	Length of the jump 30 == 1 second
var KNOCK_MAX_TIME = 30	#	Length of the self knock


func _ready():
	set_fixed_process(true)
	Player = get_node("../../../../Player_Character")
	
func _fixed_process(delta):
	###	ADD GRAVITY EACH PHYSICS FRAME
	if in_Water:
		velocity.y += 100 * delta 
	else:
		velocity.y += GRAVITY * delta
	
	###	MOVE BY VELOCITY
	move_and_slide(velocity, FLOOR_NORMAL, SLOPE_FRICTION)
	
	###	ON GROUND
	if is_move_and_slide_on_floor():
		velocity.y = 0
	elif in_Water:
		AIR_TIME = 0
	
	###	FOLLOW PLAYER IF IN DISTANCE
	if get_pos().distance_to(Player.get_pos()) < 150 && can_move:
		###	Check the Y axis difference between self and player
		if abs(Player.get_pos().y - get_pos().y) < 80:
			_follow_player(Player, self)
	
	###	ON COLLISION WITH BOX, PUSH IT
	if is_colliding():
		var groups = get_collider().get_groups()
		###	If colliding with box, make it target, call function push crate
		if groups.has("box"):
			var normal = get_collision_normal()
			if normal == Vector2(-1, 0) or normal == Vector2(1, 0):
				var target = get_collider()
				_push_crate(target)
				is_jumping = true
		###	If colliding with player, make him target, call function push character
		if groups.has("player"):
			var target = get_collider()
			_push_character(target)
			can_move = false
		###	Jump if blocked by a wall
		if groups.has("solid"):
			var normal = get_collision_normal()
			if normal == Vector2(-1, 0) or normal == Vector2(1, 0):
				is_jumping = true
			else:
				pass
		
	###	AFTER HITTING PLAYER, GOES INTO CAN_MOVE FALSE
	if can_move == false:
		MOVE_PAUSE_TIMER += 1
		###	REST FOR MOVE_PAUSE_MAX AND GET MOVING AGAIN
		if MOVE_PAUSE_TIMER >= MOVE_PAUSE_MAX:
			can_move = true
			MOVE_PAUSE_TIMER = 0
		
	###	AFTER BEING BLOCKED BY A WALL, JUMP
	if is_jumping == true && AIR_TIME <= AIR_TIME_MAX:
		velocity.y = -1
		var motion = velocity * JUMP_FORCE * delta
		move(motion)
		AIR_TIME += 1
		###	IF TIMER IS MORE THAN MAX TIME, DONT ALLOW TO JUMP
		if AIR_TIME >= AIR_TIME_MAX:
			is_jumping = false
	###	AFTER 2 SECONDS RESET JUMP TIMER
	if AIR_TIME >= AIR_TIME_MAX:
		TIME_COUNTER += 1
		if TIME_COUNTER >= 60:
			AIR_TIME = 0
			TIME_COUNTER = 0
		
	
	###	WHEN HIT BY ENEMY, IS BEING KNOCKED, THE KNOCK CODE
	if BEING_KNOCKED == true:
		###	CHECKS FOR THE KNOCK DURATION
		if KNOCK_TIMER < KNOCK_MAX_TIME:
			###	MOVING IN THE DIRECTION AND VELOCITY FROM KNOCK FUNCTION IN CHARACTERS.GD
			move_to(get_pos() + (knockVelocity))
			KNOCK_TIMER += 1
		###	RESETS TIMER AND DISABLES THE BEING KNOCKED STATE
		elif KNOCK_TIMER >= KNOCK_MAX_TIME:
			BEING_KNOCKED = false
			KNOCK_TIMER = 0