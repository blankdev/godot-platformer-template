extends "res://Scripts/Characters.gd"

#	Physics
var velocity = Vector2()
var knockVelocity = Vector2()
#	States
var can_jump = false
var can_attack = true
var in_Water = false
var BEING_KNOCKED = false
#	Timers
var AIR_TIME = 0
var KNOCK_TIMER = 0
var ATTACK_TIMER = 0
const KNOCK_MAX_TIME = 30 #	THE LENGTH OF THE KNOCK 30 == 1 second
const ATTACK_MAX_TIME = 60 #	ATTACK SPEED 30 == 1 second
const AIR_TIME_MAX = 10 #	LENGTH OF JUMP 30 == 1 second
#	Targets
var Enemy

func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):
	###	ADD GRAVITY EACH PHYSICS FRAME
	if in_Water:
		velocity.y += 100 * delta 
	else:
		velocity.y += GRAVITY * delta
	
	###	MOVE BY VELOCITY
	move_and_slide(velocity, FLOOR_NORMAL, SLOPE_FRICTION)
	
	###	ON GROUND
	if is_move_and_slide_on_floor():
		AIR_TIME = 0
		velocity.y = 0
	elif in_Water:
		AIR_TIME = 0
	else:
		AIR_TIME += 1
	
	###	CAN JUMP
	can_jump = AIR_TIME < AIR_TIME_MAX
	
	###	MOVEMENT
	var movement = 0
	
	if Input.is_action_pressed("KEY_A"):
		movement -= 1
		get_node("Attack_Hitbox").set_rotd(180)
	if Input.is_action_pressed("KEY_D"):
		movement += 1
		get_node("Attack_Hitbox").set_rotd(0)
	
	movement *= MOVEMENT_SPEED
	
	###	LERP HORIZONTAL VELOCITY
	velocity.x = lerp(velocity.x, movement, ACCELERATION)
	
	###	JUMP CODE
	if Input.is_action_pressed("KEY_SPACE") && can_jump:
		var jump = 0
		jump -= 1
		jump *= JUMP_FORCE
		velocity.y = lerp(velocity.y, jump, ACCELERATION)
	
	###	ON COLLISION WITH BOX, PUSH IT
	if is_colliding():
		var groups = get_collider().get_groups()
		if groups.has("box"):
			var target = get_collider()
			_push_crate(target)
		
	###	WHEN HIT BY ENEMY, IS BEING KNOCKED, THE KNOCK CODE
	if BEING_KNOCKED == true:
		###	CHECKS FOR THE KNOCK DURATION
		if KNOCK_TIMER < KNOCK_MAX_TIME:
			###	MOVING IN THE DIRECTION AND VELOCITY FROM KNOCK FUNCTION IN CHARACTERS.GD
			move_to(get_pos() + (knockVelocity))
			KNOCK_TIMER += 1
		###	RESETS TIMER AND DISABLES THE BEING KNOCKED STATE
		elif KNOCK_TIMER >= KNOCK_MAX_TIME:
			BEING_KNOCKED = false
			KNOCK_TIMER = 0
	
	###	On press of key, call the attack/push function
	if Input.is_action_pressed("KEY_F") && can_attack:
		_push_enemy(Enemy)
		can_attack = false
	
	###	If can't attack, count attack speed
	if !can_attack:
		ATTACK_TIMER += 1
		if ATTACK_TIMER >= ATTACK_MAX_TIME:
			can_attack = true
			ATTACK_TIMER = 0
	
###	Hitbox trigger enter
func _on_Attack_Hitbox_body_enter( body ):
	var groups = body.get_groups()
	if groups.has("Enemy"):
		Enemy = body

###	Hitbox trigger exit
func _on_Attack_Hitbox_body_exit( body ):
	var groups = body.get_groups()
	if groups.has("Enemy"):
		Enemy = null
