extends Node

var Current_Level
var New_Level
var Levels = {
	"Main_Menu": preload("res://Scenes/Main_Menu.tscn"),
	"Level1": preload("res://Scenes/Levels/Level1.tscn")
	}
var Player_Character

func _ready():
	Current_Level = get_node("Draw/Level")
	_load_level("Main_Menu")
func _load_level(level):
	if level in Levels:
		var old_level = null
		if Current_Level.get_child_count() > 0:
			old_level = Current_Level.get_child(0)
		if old_level != null:
			Current_Level.remove_child(old_level)
		
		var New_Level = Levels[level].instance()
		Current_Level.add_child(New_Level)
		var Player_Character = get_node("Draw/Player_Character")
		if Player_Character != null:
			Player_Character.set_global_pos(New_Level.get_node("Spawn").get_global_pos())
		else:
			pass
	else:
		print("ERROR Cannot load Level: ", level)
	