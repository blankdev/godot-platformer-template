extends Node

var New_Game
var Player_Character = preload("res://Scenes/Characters/Player_Character.tscn")
var Draw
var Game

func _ready():
	New_Game = get_node("New_Game")
	Draw = get_node("../../../Draw")
	Game = get_node("../../../../root")
	

func _on_New_Game_pressed():
	###	INSTANCE PLAYER CHARACTER INTO THE GAME
	var Character = Player_Character.instance()
	Draw.add_child(Character)
	###	LOAD THE FIRST LEVEL OF THE GAME
	Game._load_level("Level1")
	
	
